igscp_stats
===========

.. toctree::
   :maxdepth: 4

   event_stats
   magnitude_variation
   magnitude_variation_plotly
   run_event_stats
   run_event_variation
